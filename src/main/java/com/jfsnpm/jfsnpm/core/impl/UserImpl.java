package com.jfsnpm.jfsnpm.core.impl;


import com.jfinal.core.Controller;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;
import com.jfsnpm.jfsnpm.core.AppConfig;
import com.jfsnpm.jfsnpm.core.dao.User;
import com.jfsnpm.jfsnpm.core.global.IUser;
import com.jfsnpm.jfsnpm.core.util.AppHelper;

public class UserImpl implements IUser {

	@Override
	public User checkToken(String token,Controller c,boolean remote) {
		if(AppHelper.isEmpty(token)) return null;
		User user = null;
		Cache cache = Redis.use();
		if(remote){
			//远程token认证
			user = AppConfig.userRemoteimpl.checkToken(token,c);
			if(user != null){//远程验证成功
				//需要写入本地认证数据
				String userNo = user.getUserNo();
				//单点登录，清除原有的本地token
				if(AppConfig.properties.getProperty("SSO", "true").equals("true")){
					if(cache.exists(userNo)){
						String oldToken = cache.get(userNo);
						if(cache.exists(oldToken)){
							cache.del(oldToken);
						}
					}
				}
				user.setToken(AppHelper.getNewToken());//生成使用本地token
				cache.setex(user.getToken(), Integer.valueOf(AppConfig.properties.getProperty("timeout")),user);
				cache.setex(user.getUserNo(), Integer.valueOf(AppConfig.properties.getProperty("timeout")), user.getToken());
			}
		}else{
			//本地token验证
			if(cache.exists(token)){
				user = getUserByToken(token);
			}
		}
		return user;
	}

	@Override
	public User getUser(String userNo) {
		if(AppHelper.isEmpty(userNo)) return null;
		Cache cache = Redis.use();
		String token = cache.get(userNo);
		User user = null;
		if(!AppHelper.isEmpty(token)){
			user = cache.get(token);
			if(user != null){
				return user;
			}
		}
		return AppConfig.userRemoteimpl.getUser(userNo);
	}
	@Override
	public User checkUser(String user, String psw) {
		if(AppHelper.isEmpty(user)||AppHelper.isEmpty(psw)) return null;
		Cache cache = Redis.use();
		User u = AppConfig.userRemoteimpl.checkUser(user, psw);
		if(u != null){//验证通过后写入本地认证数据
			String userNo = u.getUserNo();
			//单点登录，清除原有的本地token
			if(AppConfig.properties.getProperty("SSO", "true").equals("true")){
				if(cache.exists(userNo)){
					String oldToken = cache.get(userNo);
					if(cache.exists(oldToken)){
						cache.del(oldToken);
					}
				}
			}
			u.setToken(AppHelper.getNewToken());//生成使用本地token
			cache.setex(u.getToken(),  Integer.valueOf(AppConfig.properties.getProperty("timeout")), u);
			cache.setex(u.getUserNo(), Integer.valueOf(AppConfig.properties.getProperty("timeout")), u.getToken());
		}
		return u;
	}
	@Override
	public User getUserByToken(String token){
		if(AppHelper.isEmpty(token)) return null;
		Cache cache = Redis.use();
		User user = cache.get(token);
		return user;
	}

	@Override
	public boolean clearToken(String token) {
		if(AppHelper.isEmpty(token)) return true;
		Cache cache = Redis.use();
		if(cache.exists(token)){
			User u = getUserByToken(token);
			if(u!=null){
				String userNo = u.getUserNo();
				if(!AppHelper.isEmpty(userNo)){
					cache.del(userNo);
				}
			}
			cache.del(token);
		}
		return true;
	}

	@Override
	public User getUserById(String userId) {
		return AppConfig.userRemoteimpl.getUserById(userId);
	}
	/**
	 * 建立acc令牌
	 * @return
	 */
	@Override
	public String createAccToken(){
		String acctoken = "ACC"+AppHelper.getUUID();
		Redis.use().setex(acctoken, 900, "");
		return acctoken;
	}
	/**
	 * 验证acc令牌
	 * @param accToken
	 * @return
	 */
	@Override
	public boolean checkAccToken(String accToken){
		if(AppHelper.isEmpty(accToken)) return false;
		Cache cache = Redis.use();
		if(cache.exists(accToken)){
			cache.del(accToken);
			return true;
		}
		return false;
	}

}
