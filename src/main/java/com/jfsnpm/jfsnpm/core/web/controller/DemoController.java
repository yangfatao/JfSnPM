package com.jfsnpm.jfsnpm.core.web.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.matchers.GroupMatcher;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;
import com.jfsnpm.jfsnpm.core.AppConfig;
import com.jfsnpm.jfsnpm.core.util.AppHelper;
import com.jfsnpm.jfsnpm.core.util.JfsnpmException;
import com.jfsnpm.jfsnpm.core.util.MessageHelper;
import com.jfsnpm.jfsnpm.demo.txa;
import com.jfsnpm.jfsnpm.demo.txb;
import com.jfsnpm.jfsnpm.plugin.bjui.Bjui;
import com.jfsnpm.jfsnpm.plugin.bjui.BjuiRender;
import com.jfsnpm.jfsnpm.plugin.quartz.QuartzPlugin;
import com.jfsnpm.jfsnpm.plugin.quartz.ScheduleJob;

public class DemoController extends Controller {
	public void language(){
	}
	public void tree(){
	}
	@Before(Tx.class)
	public void testtx() throws Exception{
		txa.savea();
		txb.saveb();
		renderText("ok");
	}
	public void sendmsg() throws UnsupportedEncodingException{
		String title = "通知";
		String to = getPara("to");
		String msg = URLDecoder.decode(getPara("msg"),"utf-8");
		if(MessageHelper.sendNotify(title, to, msg)){
			renderText("ok");
		}else{
			renderText("error");
		}
	}
	public void timeout(){
		render(BjuiRender.timeout());
	}
	public void getjson(){
		renderJson(Db.find("SELECT * FROM jfsnpm_role"));
	}
	public void success(){
		render(BjuiRender.success("成功"));
	}
	public void editsuccess(){
		String json = getPara("json");
		json = json.replaceAll("\"addFlag\":true", "\"addFlag\":false");
		renderJson(json);
	}
	public void datagrid_remote(){
	}
	public void datagrid_remote_get(){
		renderJson(Bjui.use().page("select * from jfsnpm_logs order by id desc", this));
	}
	public void datagrid_remote_get_export(){
		String[] headers = {"id","用户","URL"};
		String[] columns = {"id","userId","url"};
		render(AppHelper.exportExcel(Bjui.use().find("select * from jfsnpm_logs order by id desc", "/Demo/datagrid_remote_get", this),
				"DEMO导出LOGS", "all", headers,columns));
	}
	public void testhtml(){
		
	}
	public void testinclude(){
		setAttr("testcontent", "我是参数");
	}
	public void testredis(){
		//检查各服务状态
		Logger.getLogger(this.getClass()).warn("-----------------");
		Logger.getLogger(this.getClass()).warn("-----------------");
		Logger.getLogger(this.getClass()).warn("-----------------");
		Logger.getLogger(this.getClass()).warn("--开始检查Redis");
		Cache cache = Redis.use();
		Logger.getLogger(this.getClass()).warn("--获取cache成功");
		cache.set("testredis", "success"+AppHelper.getNow());
		Logger.getLogger(this.getClass()).warn("--写入数据成功");
		String testredis = cache.get("testredis");
		Logger.getLogger(this.getClass()).warn("--读取数据成功");
		if(cache.exists("testredis")){
			Logger.getLogger(this.getClass()).warn("--确认数据成功");
			cache.del("testredis");
			Logger.getLogger(this.getClass()).warn("--删除数据成功");
		}
		Logger.getLogger(this.getClass()).warn("--数据："+testredis);
		Logger.getLogger(this.getClass()).warn("-----------------");
		Logger.getLogger(this.getClass()).warn("-----------------");
		Logger.getLogger(this.getClass()).warn("-----------------");
		renderText("检查成功~");
	}
	public void quartzlist() throws SchedulerException{
		Scheduler scheduler = QuartzPlugin.getScheduler();
		GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();
		Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);
		List<ScheduleJob> jobList = new ArrayList<ScheduleJob>();
		for (JobKey jobKey : jobKeys) {
		    List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);
		    for (Trigger trigger : triggers) {
		        ScheduleJob job = new ScheduleJob();
		        job.setJobName(jobKey.getName());
		        job.setJobGroup(jobKey.getGroup());
		        job.setDesc("触发器:" + trigger.getKey());
		        Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
		        job.setJobStatus(triggerState.name());
		        if (trigger instanceof CronTrigger) {
		            CronTrigger cronTrigger = (CronTrigger) trigger;
		            String cronExpression = cronTrigger.getCronExpression();
		            job.setCronExpression(cronExpression);
		        }
		        jobList.add(job);
		    }
		}
		setAttr("joblist", jobList);
	}
}
