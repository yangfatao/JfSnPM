package com.jfsnpm.jfsnpm.core.dao;

import java.io.Serializable;

public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2241190368616767279L;
	private String token;
	private String userId;
	private String userNo;
	private String userName;
	private String userMail;
	private String userOtherKey=" ";
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserNo() {
		return userNo;
	}
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserMail() {
		return userMail;
	}
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}
	public String getUserOtherKey() {
		return userOtherKey;
	}
	public void setUserOtherKey(String userOtherKey) {
		this.userOtherKey = userOtherKey;
	}
	
	
}
